﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConverterDataType
{
    public static class ParseValue
    {

        public static bool ParseType(string value, Type type, ref object targetValue)
        {
            try
            {
                switch (type.Name.ToString())
                {
                    case "String":
                    case "Char":
                        targetValue = (string)value;
                        return true;
                    case "Int32":
                    case "Decimal":
                    case "Double":
                    case "Int16":
                    case "Int64":
                    case "UInt16":
                    case "UInt32":
                    case "UInt64":
                        Int32 number;
                        if (Int32.TryParse(value, out number))
                        {
                            targetValue = number;
                            return true;
                        }
                        return false;
                    case "DateTime":
                    case "TimeSpan":
                        DateTime dateTime;
                        if (DateTime.TryParse(value, out dateTime))
                        {
                            targetValue = dateTime;
                            return true;
                        }
                        return true;
                    default:
                        return false;
                }
            } catch(Exception ex)
            {
                return false;
            }
        }

        public static string ParseInputToQuery(object value)
        {
            try
            {
                switch (value.GetType().Name)
                {
                    case "String":
                    case "Char":
                        return $"N'{value}'";
                    case "Int32":
                    case "Decimal":
                    case "Double":
                    case "Int16":
                    case "Int64":
                    case "UInt16":
                    case "UInt32":
                    case "UInt64":
                    case "Int":
                        return $"{value}";
                    case "DateTime":
                    case "TimeSpan":
                        return $"'{value}'";
                    default:
                        return $"{value}";
                }
            }
            catch (Exception)
            {
                return "";
            }
           
        }
    }
}
