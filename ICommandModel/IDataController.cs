﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICommandModel
{
    public interface IDataController
    {
        bool Login(string username, string password);
        bool Add(string tableName, string value);
        string Update(string tableName, string setClause, string condition);
        string Delete(string tableName, string condition);
        public void createSessionTable();
        List<string> ReadAllDataTableName();
        DataTable ReadTable(string nameTable);
        bool CreateSessionRecord(object[] inputs);
        DataTable ExecuteRawQuery(string queryString);

        List<string> GetPrimaryKey(string tableName);

        string UpdateRecord(string tableName, List<Dictionary<string, dynamic>> dataChanged, List<Dictionary<string, dynamic>> primaryKeyValue);
    }
}
