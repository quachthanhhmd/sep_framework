﻿using System;
using System.Data;

namespace ICommandModel
{
    public interface IDataModel
    {

        void AttachConnectionString(string connectionString);
        public string getDatabaseName();
        DataTable GetData(string tableName, string condition);
        bool Add(string tableName, string value);
        string Update(string tableName, string setClause, string condition);

        // return "" if complete message if false
        string Delete(string tableName, string condition);
        bool CheckIsExist(string queryString);
        bool ExecuteQuery(string createTable);
        DataTable ExecuteRawQuery(string queryString);
    }
}
