﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICommandModel
{
    public abstract class IModel
    {
        public abstract string ModelType { get; }
        public IDataController DataController;
        public IDataModel DataModel;

        public abstract void AttachModelToController(IDataModel model);
        public abstract void AttachConnection(string connectionString);
    }
}
