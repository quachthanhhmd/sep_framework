﻿using ICommandModel;
using PasswordAlgorithm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySQL.Controllers
{
    public class MySqlController : IDataController
    {
        private IDataModel dataModel;
        private string _databaseName;

        public MySqlController()
        {

        }

        public MySqlController(IDataModel model)
        {
            dataModel = model;
            _databaseName = dataModel.getDatabaseName();
        }

        public List<string> GetPrimaryKey(string nameTable)
        {
            string sql = String.Format("SHOW KEYS FROM {0} WHERE Key_name = 'PRIMARY'", nameTable);
            try
            {
                DataTable result = dataModel.ExecuteRawQuery(sql);

                List<string> primaryKeyList = new List<string>();
                if (result.Rows.Count > 0)
                {
                    for (int i = 0; i < result.Rows.Count; i++)
                    {
                        primaryKeyList.Add(result.Rows[i]["COLUMN_NAME"].ToString());
                    }

                }

                return primaryKeyList;
            }
            catch (Exception)
            {

            }
            return new List<string>();
        }

        public void createSessionTable()
        {
            string databaseName = dataModel.getDatabaseName();
            DataTable dataTable = dataModel.GetData("INFORMATION_SCHEMA.TABLES", string.Format("table_schema='{0}' AND table_name='session'", databaseName));
            if (dataTable.Rows.Count == 0)
            {
                var createTable = "CREATE TABLE session (id INT AUTO_INCREMENT PRIMARY KEY, username VARCHAR(30), password VARCHAR(30) NOT NULL, isLogin bit)";
                dataModel.ExecuteQuery(createTable);
            }
        }

        private bool Authen(string username, string password)
        {
            DataTable data = dataModel.GetData("session", string.Format("username='{0}'", username));

            if (data.Rows.Count != 0)
            {
                string u = data.Rows[0]["username"].ToString();
                string p = Crypt.Decrypt(data.Rows[0]["password"].ToString());
                return username == u && password == p;
            }
            return false;
        }

        public bool Login(string username, string password)
        {
            if (Authen(username, password))
            {
                if (dataModel.Update("session", "isLogin=1", string.Format("username='{0}'", username)) == "")
                    return true;
            }
            return false;
        }

        public List<string> ReadAllDataTableName()
        {
            try
            {
                string queryString = string.Format("SELECT table_name FROM information_schema.tables WHERE table_schema = '{0}';", _databaseName);
                DataTable tableNameData = dataModel.ExecuteRawQuery(queryString);

                if (tableNameData.Rows.Count > 0)
                {
                    List<string> tableNamelist = new List<string>();
                    foreach (DataRow row in tableNameData.Rows)
                    {
                        if (row["table_name"].ToString() != "session")
                            tableNamelist.Add(row["table_name"]?.ToString());
                    }
                    return tableNamelist;
                }
                return new List<string>();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public DataTable ReadTable(string nameTable)
        {
            string condition = "1 = 1";
            DataTable result = dataModel.GetData(nameTable, condition);
            return result;
        }

        public bool CreateSessionRecord(object[] inputs)
        {
            string username = (string)inputs[0];
            string password = (string)inputs[1];
            string encryptedPassword = Crypt.Encrypt(password);

            string queryString = string.Format(string.Format("INSERT INTO SESSION(username, password, isLogin) values ('{0}', '{1}', 0)", username, encryptedPassword));
            try
            {
                return dataModel.ExecuteQuery(queryString);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable ExecuteRawQuery(string queryString)
        {
            return dataModel.ExecuteRawQuery(queryString);
        }

        public bool Add(string tableName, string value)
        {
            return dataModel.Add(tableName, value);
        }

        public string Update(string tableName, string setClause, string condition)
        {
            return dataModel.Update(tableName, setClause, condition);
        }

        public string Delete(string tableName, string condition)
        {
            return dataModel.Delete(tableName, condition);
        }

        public string UpdateRecord(string tableName, List<Dictionary<string, dynamic>> dataChanged, List<Dictionary<string, dynamic>> primaryKeyValue)
        {
            for (int i = 0; i < dataChanged.Count; i++)
            {
                string sql = "update " + tableName + " set ";

                for (int j = 0; j < dataChanged[i].Count; j++)
                {

                    if (j < dataChanged[i].Count - 1)
                    {
                        sql += (dataChanged[i].ElementAt(j).Key + " = N'" + dataChanged[i].ElementAt(j).Value + "', ");
                    }
                    else
                    {
                        sql += (dataChanged[i].ElementAt(j).Key + " = N'" + dataChanged[i].ElementAt(j).Value + "'");
                    }
                }

                string primaryStringQuery = " where ";

                for (int j = 0; j < primaryKeyValue[i].Count; j++)
                {
                    primaryStringQuery += $"{primaryKeyValue[i].ElementAt(j).Key} = N'{primaryKeyValue[i].ElementAt(j).Value}'";
                }

                sql += primaryStringQuery;

                try
                {
                    dataModel.ExecuteQuery(sql);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return "";
        }
    }
}