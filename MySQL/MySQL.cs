﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;
using MySQL.Controllers;
using MySQL.Models;

namespace MySQL
{
    public class SqlServer : IModel
    {

        public SqlServer()
        {
            DataController = new MySqlController();
            DataModel = MySqlModel.GetInstance();
        }

        public override string ModelType => "MySql";

        public override void AttachConnection(string connectionString)
        {
            DataModel.AttachConnectionString(connectionString);
        }

        public override void AttachModelToController(IDataModel model)
        {
            DataController = new MySqlController(model);
        }

    }
}
