﻿using ICommandModel;
using PasswordAlgorithm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSQL.Controllers
{
    public class PostgresController : IDataController
    {
        private IDataModel dataModel;
        private string _databaseName;

        public PostgresController()
        {
        }

        public PostgresController(IDataModel model)
        {
            dataModel = model;
            _databaseName = dataModel.getDatabaseName();
        }

        public void createSessionTable()
        {
            string databaseName = dataModel.getDatabaseName();
            DataTable dataTable = dataModel.GetData("information_schema.tables", string.Format("table_catalog='{0}' and table_name='session'", databaseName));
            if (dataTable.Rows.Count == 0)
            {
                var createTable = "CREATE TABLE session(id SERIAL PRIMARY KEY, username VARCHAR(30), password varchar(30), isLogin bit)";
                dataModel.ExecuteQuery(createTable);
            }
        }

        private bool Authen(string username, string password)
        {
            DataTable data = dataModel.GetData("session", string.Format("username='{0}'", username));

            if (data.Rows.Count != 0)
            {
                string u = data.Rows[0]["username"].ToString();
                string p = Crypt.Decrypt(data.Rows[0]["password"].ToString());
                return username == u && password == p;
            }
            return false;
        }

        public bool Login(string username, string password)
        {
            if (Authen(username, password))
            {
                if (dataModel.Update("session", "islogin=B'1'", string.Format("username='{0}'", username)) == "")
                    return true;
            }
            return false;
        }

        public List<string> ReadAllDataTableName()
        {
            try
            {
                string queryString = $"SELECT * from pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND " +
                    $"schemaname != 'information_schema'; ";
                DataTable tableNameData = dataModel.ExecuteRawQuery(queryString);

                if (tableNameData.Rows.Count > 0)
                {
                    List<string> tableNamelist = new List<string>();
                    foreach (DataRow row in tableNameData.Rows)
                    {
                        if (row["tablename"].ToString() != "session")
                            tableNamelist.Add(row["tablename"]?.ToString());
                    }
                    return tableNamelist;
                }
                return new List<string>();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public DataTable ReadTable(string nameTable)
        {
            string condition = "1 = 1";
            DataTable result = dataModel.GetData(nameTable, condition);
            return result;
        }

        public bool CreateSessionRecord(object[] inputs)
        {
            string username = (string)inputs[0];
            string password = (string)inputs[1];
            string encryptedPassword = Crypt.Encrypt(password);

            string queryString = string.Format(string.Format("INSERT INTO SESSION(username, password, isLogin) values ('{0}', '{1}', B'0')", username, encryptedPassword));
            try
            {
                return dataModel.ExecuteQuery(queryString);
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public DataTable ExecuteRawQuery(string queryString)
        {
            return dataModel.ExecuteRawQuery(queryString);
        }
        public bool Add(string tableName, string value)
        {
            return dataModel.Add(tableName, value);
        }

        public string Update(string tableName, string setClause, string condition)
        {
            return dataModel.Update(tableName, setClause, condition);
        }

        public string Delete(string tableName, string condition)
        {
            return dataModel.Delete(tableName, condition);
        }

        public List<string> GetPrimaryKey(string tableName)
        {
            string sql = $"SELECT pg_attribute.attname, format_type(pg_attribute.atttypid, pg_attribute.atttypmod) " +
                $"FROM pg_index, pg_class, pg_attribute, pg_namespace WHERE " +
                $"pg_class.oid = '{tableName}'::regclass AND indrelid = pg_class.oid AND nspname = 'public' AND " +
                $"pg_class.relnamespace = pg_namespace.oid AND pg_attribute.attrelid = pg_class.oid AND " +
                $"pg_attribute.attnum = any(pg_index.indkey) AND indisprimary";
            try
            {
                DataTable result = dataModel.ExecuteRawQuery(sql);

                List<string> primaryKeyList = new List<string>();
                if (result.Rows.Count > 0)
                {
                    for (int i = 0; i < result.Rows.Count; i++)
                    {
                        primaryKeyList.Add(result.Rows[i]["attname"].ToString());
                    }

                }

                return primaryKeyList;
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public string UpdateRecord(string tableName, List<Dictionary<string, dynamic>> dataChanged, List<Dictionary<string, dynamic>> primaryKeyValue)
        {
            for (int i = 0; i < dataChanged.Count; i++)
            {
                string sql = "update " + tableName + " set ";

                for (int j = 0; j < dataChanged[i].Count; j++)
                {

                    if (j < dataChanged[i].Count - 1)
                    {
                        sql += (dataChanged[i].ElementAt(j).Key + " = N'" + dataChanged[i].ElementAt(j).Value + "', ");
                    }
                    else
                    {
                        sql += (dataChanged[i].ElementAt(j).Key + " = N'" + dataChanged[i].ElementAt(j).Value + "'");
                    }
                }

                string primaryStringQuery = " where ";

                for (int j = 0; j < primaryKeyValue[i].Count; j++)
                {
                    primaryStringQuery += $"{primaryKeyValue[i].ElementAt(j).Key} = '{primaryKeyValue[i].ElementAt(j).Value}'";
                }

                sql += primaryStringQuery;

                try
                {
                    dataModel.ExecuteQuery(sql);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return "";
        }
    }
}
