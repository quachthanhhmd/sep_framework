﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;
using Npgsql;

namespace PostgreSQL.Models
{
    public class PostgresModel : IDataModel
    {
        private static PostgresModel _instance;

        public static PostgresModel GetInstance()
        {
            if (_instance == null)
            {
                _instance = new PostgresModel();
            }
            return _instance;
        }

        public PostgresModel()
        {

        }

        private NpgsqlConnection connection = null;

        public PostgresModel(string url)
        {
            if (_instance == null)
            {
                _instance = new PostgresModel();
                _instance.connection = new NpgsqlConnection(url);
            }
        }

        public void AttachConnectionString(string url)
        {
            if (_instance == null)
            {
                _instance = new PostgresModel(url);
                return;
            }
            _instance.connection = new NpgsqlConnection(url);
        }

        public string getDatabaseName()
        {
            NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder(_instance.connection.ConnectionString);
            return builder.Database;
        }

        public bool CheckIsExist(string queryString)
        {
            //SqlCommand query = new SqlCommand(queryString, _instance.connection);
            //_instance.connection.Open();

            //SqlDataReader result = query.ExecuteReader();

            //bool isComplete = result.Read();

            //_instance.connection.Close();

            //return isComplete;
            return false;
        }

        public bool ExecuteQuery(string sql)
        {
            try
            {
                DbCommand sql_query = new NpgsqlCommand(sql, _instance.connection);

                if (_instance.connection.State != ConnectionState.Open)
                    _instance.connection.Open();
                sql_query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                throw ex;
                return false;
            }

            _instance.connection.Close();
            return true;
        }

        public DataTable GetData(string tableName, string condition)
        {
            try
            {
                string queryString = $"select * from {tableName} where {condition}";
                _instance.connection.Open();
                NpgsqlDataAdapter adapt = new NpgsqlDataAdapter(queryString, _instance.connection);

                DataTable dataValue = new DataTable();
                adapt.Fill(dataValue);
                _instance.connection.Close();
                return dataValue;
            }
            catch (Exception)
            {
                _instance.connection.Close();
            }
            return null;
        }
        public bool Add(string tableName, string value)
        {
            try
            {
                int secondClosePosition = 0;
                for (int i = 0; i < tableName.Length; i++)
                {
                    if (tableName[i] == ')')
                        break;
                    secondClosePosition++;
                }
                string temp1String = tableName.Substring(0, secondClosePosition);
                temp1String = temp1String.Replace("\"", "");

                string newTableNameString = temp1String + tableName.Substring(secondClosePosition);
                string sql = $"insert into {newTableNameString} values {value}";
                DbCommand sql_query = new NpgsqlCommand(sql, _instance.connection);

                if (_instance.connection.State != ConnectionState.Open)
                    _instance.connection.Open();

                sql_query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                return false;
            }

            _instance.connection.Close();
            return true;
        }

        public string Update(string tableName, string setClause, string condition)
        {
            try
            {
                string sql = $"update {tableName} set {setClause} where {condition}";
                DbCommand sql_query = new NpgsqlCommand(sql, _instance.connection);

                if (_instance.connection.State != ConnectionState.Open)
                    _instance.connection.Open();

                sql_query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                return ex.Message;
            }

            _instance.connection.Close();
            return "";
        }

        public string Delete(string tableName, string condition)
        {
            try
            {
                string sql = $"delete from {tableName} where {condition}";
                DbCommand sql_query = new NpgsqlCommand(sql, _instance.connection);

                if (_instance.connection.State != ConnectionState.Open)
                    _instance.connection.Open();

                sql_query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                return ex.Message;
            }

            _instance.connection.Close();
            return "";
        }

        public DataTable ExecuteRawQuery(string queryString)
        {
            try
            {
                _instance.connection.Open();
                NpgsqlDataAdapter adapt = new NpgsqlDataAdapter(queryString, _instance.connection);

                DataTable dataValue = new DataTable();
                adapt.Fill(dataValue);
                _instance.connection.Close();
                return dataValue;
            }
            catch (Exception)
            {
                _instance.connection.Close();
            }
            return new DataTable();
        }
    }
}
