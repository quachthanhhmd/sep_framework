﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;
using PostgreSQL.Controllers;
using PostgreSQL.Models;

namespace PostgreSQL
{
    public class PostgreSQL : IModel
    {
        public PostgreSQL()
        {
            base.DataController = new PostgresController();
            base.DataModel = PostgresModel.GetInstance();
        }

        public override string ModelType => "Postgres";

        public override void AttachConnection(string connectionString)
        {
            DataModel.AttachConnectionString(connectionString);
        }

        public override void AttachModelToController(IDataModel model)
        {
            DataController = new PostgresController(model);
        }

    }
}
