﻿using ICommandModel;
using SEPFramework.Model;
using SEPFramework.TContainer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEPFramework.GeneralController
{
    public class DataController : GeneralModel, Controller.IAdapterController
    {
        private static DataController? _controllerInstance;


        public static DataController getControllerInstance()
        {
            if (_controllerInstance == null)
                _controllerInstance = Installer.contaier.GetService<DataController>();
            return _controllerInstance;
        }

        public DataController CreateControllerInstance(IDataController controller)
        {
            GeneralModel.CreateInstance(controller);
            return getControllerInstance();
        }

        public bool CreateSessionRecord(object[] inputs)
        {
            try
            {
                if (Controller.CreateSessionRecord(inputs))
                    return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<string> GetAllTableName()
        {
            try
            {
                return Controller.ReadAllDataTableName();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public DataTable GetDataTable(string tableName)
        {
            try
            {
                DataTable dataTable = Controller.ReadTable(tableName);

                return dataTable;
            } catch (Exception)
            {

            }
            return new DataTable();
        }

        public List<string> GetPrimaryKey(string tableName)
        {
            try
            {
                return Controller.GetPrimaryKey(tableName);
            } catch (Exception)
            {

            }
            return new List<string>();
        }

        public string UpdateData(string tableName, List<Dictionary<string, dynamic>> dataChange, List<Dictionary<string, dynamic>> primaryList)
        {
            try
            {
                return Controller.UpdateRecord(tableName, dataChange, primaryList);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
