﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEPFramework.Controller
{
    public interface IAdapterController
    {
        List<string> GetAllTableName();

        DataTable GetDataTable(string tableName);
        bool CreateSessionRecord(object[] input);

        List<string> GetPrimaryKey(string tableName);

        string UpdateData(string tableName, List<Dictionary<string, dynamic>> dataChange, List<Dictionary<string, dynamic>> primaryList);
    }
}
