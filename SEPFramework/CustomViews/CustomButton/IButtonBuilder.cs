﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SEPFramework.CustomViews.CustomButton
{
    public interface IButtonBuilder
    {
        IButtonBuilder AddWidth(double width);
        IButtonBuilder AddHeight(double height);

        IButtonBuilder AddMargin(Thickness margin);

        IButtonBuilder AddContent(string content);

        IButtonBuilder AddClickHandle(RoutedEventHandler myMethodName);

        Button Build();
    }
}
