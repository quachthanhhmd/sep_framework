﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using SEPFramework.CustomViews.CustomButton;

namespace SEPFramework.CustomViews.CustomButton
{
    public class MyButton:  Button, IButtonBuilder
    {
        private double _width;
        private double _height;
        private Thickness _margin;
        private string? _content;
        private RoutedEventHandler? _buttonClick;

         public IButtonBuilder AddWidth(double width)
        {
            _width = width;
            return this;
        }

        public IButtonBuilder AddHeight(double height)
        {
          _height = height;
           return this;
        }

        public IButtonBuilder AddMargin(Thickness margin)
        {
            _margin = margin;
            return this;
        }

        public IButtonBuilder AddContent(string content)
        {
            _content = content;
            return this;
        }

        public IButtonBuilder AddClickHandle(RoutedEventHandler myMethodName)
        {
            _buttonClick += myMethodName;
            return this;
        }

        public Button Build()
        {
            Button myButton =  new Button();
            myButton.Content = _content;
            myButton.Margin = _margin;
            myButton.Width = _width;
            myButton.Height = _height;
            if (_buttonClick != null)
                 myButton.Click += _buttonClick;
            return myButton;
        }
    }
}
