﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;

namespace SEPFramework.Factory
{
    public static class ModelFactory
    {
        private static Dictionary<string, IModel> _modelTypeList = new Dictionary<string, IModel>();


        public static BindingList<string> GetDBList()
        {
            BindingList<string> dbTypeList = new BindingList<string>();
            foreach (var key in _modelTypeList.Keys.ToArray())
            {
                dbTypeList.Add(key);
            }
            return dbTypeList;
        }

        public static void LoadModel()
        {
            string exePath = Assembly.GetExecutingAssembly().Location;

            string folder = Path.GetDirectoryName(exePath);
            FileInfo[] fis = new DirectoryInfo(folder).GetFiles("*.dll");

            foreach (FileInfo fileInf in fis)
            {
                var domain = AppDomain.CurrentDomain;
                Assembly assembly = domain.Load(AssemblyName.GetAssemblyName(fileInf.FullName));
                Type[] types = assembly.GetTypes();

                foreach (var type in types)
                {
                    if (type.IsClass && typeof(IModel).IsAssignableFrom(type) && !type.IsAbstract)
                    {
                        IModel model = (IModel)Activator.CreateInstance(type);

                        if (model != null && !_modelTypeList.ContainsKey(model.ModelType))
                            _modelTypeList.Add(model.ModelType, model);
                    }
                }
            }
            //var exeFolder = AppDomain.CurrentDomain.BaseDirectory;
            //var dlls = new DirectoryInfo(exeFolder).GetFiles("*.dll");

            //foreach (var dll in dlls)
            //{
            //    var assembly = Assembly.LoadFile(dll.FullName);
            //    var types = assembly.GetTypes();

            //    foreach (var type in types)
            //    {
            //       if (type.IsClass && typeof(IModel).IsAssignableFrom(type) && !type.IsAbstract)
            //        {
            //            IModel model = (IModel)Activator.CreateInstance(type);

            //            if (model != null)
            //                _modelTypeList.Add(model.ModelType, model);
            //        }
            //    }
            //}
        }


        public static IModel CreateModel(string modelType)
        {
            if (_modelTypeList.ContainsKey(modelType))
            {
                return _modelTypeList[modelType];
            }
            return null;
        }

    }
}
