﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SEPFramework.Views;
using SEPFramework.Factory;
using SEPFramework.Membership;
using ICommandModel;
using System.Diagnostics;
using SEPFramework.TContainer;

namespace SEPFramework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SEPFramework.Membership.Membership member;

        public MainWindow()
        {
            Installer.Install();
            ModelFactory.LoadModel();
            //string url = @"server=localhost;uid=root;password=123456789;database=test";
            string url = @"Host=localhost;Username=postgres;Password=123456789;Database=postgres";
            //member = new Membership.Membership(url, "Postgres");   // <-<- SqlServer / MySql / Postgres

            //string url = @"Server=localhost;Data Source=MSI\SQLEXPRESS;Initial Catalog=qldt;Integrated Security=True";
            //string url = @"Server=MSI\DB_INSTANCE;Database=test;Trusted_Connection=True;";
            //string url = @"Server=MSI\SQLEXPRESS;Database=qldt;Trusted_Connection=True;";
            //string url = @"server=localhost;uid=root;password=123456789;database=test;port=3306;charset=utf8";
            //string url = @"Data Source=DESKTOP-R3S4GT6\SQLEXPRESS;Initial Catalog=QLDT1;Integrated Security=True";
            member = SEPFramework.Membership.Membership.getInstance().CreateMembership(url, "Postgres");
        }

        private void buttonRegister_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            Window registerWindow = new RegisterWindow();
            registerWindow.Show();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            var user = username.Text;
            var pas = password.Password;

            if (this.member.Login(user, pas))
            {
                //MessageBox.Show("OKKK");

                HomeWindow home = new HomeWindow();
                this.Close();
                home.Show();
            }
            else
            {
                MessageBox.Show("Login Fail");
                // add fail
            }
        }

        private void TestHome(object sender, RoutedEventArgs e)
        {
            HomeWindow w1 = new HomeWindow();
            w1.Show();
            this.Close();
        }
    }
}
