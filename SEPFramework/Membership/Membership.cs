﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;
using SEPFramework.Factory;
using SEPFramework.Model;
using SEPFramework.TContainer;

namespace SEPFramework.Membership
{
    public class Membership : GeneralModel
    {
        private static Membership _memberShipInstance;

        public object Controler { get; internal set; }

        public static Membership getInstance()
        {
            if (_memberShipInstance == null)
                _memberShipInstance = Installer.contaier.GetService<Membership>(); 
            return _memberShipInstance;
        }

        public Membership CreateMembership(string cnnString, string dbms)
        {
            IModel dbModel = ModelFactory.CreateModel(dbms);
            dbModel.AttachConnection(cnnString);
            dbModel.AttachModelToController(dbModel.DataModel);

            GeneralModel.CreateInstance(dbModel.DataController);
            Controller.createSessionTable();
            return getInstance();
        }

        public bool Login(string username, string password)
        {
            if (Controller.Login(username, password))
            {
                return true;
            }
            return false;
        }

        public bool Register(string username, string password)
        {
            //if (Controller.Register(username, password))
            //{
            //    return true;
            //}
            return false;
        }

        public bool Logout(string username)
        {
            //if (Controller.Logout(username))
            //{
            //    return true;
            //}
            return false;
        }

        public DataTable ExecuteRawQuery(string queryString)
        {
            return Controller.ExecuteRawQuery(queryString);
        }
        public bool Add(string tableName, string value)
        {
            try
            {
                return Controller.Add(tableName, value);
            } catch (Exception)
            {
                return false;
            }
   
        }

        public string Update(string tableName, string setClause, string condition)
        {
            return Controller.Update(tableName, setClause, condition);
        }

        public string Delete(string tableName, string condition)
        {
            return Controller.Delete(tableName, condition);
        }
    }
}
