﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SEPFramework.Views
{
    public partial class HomeWindow : Window
    {

        private void TableNameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateDataGrid((string)tableNameCombobox.SelectedItem);
        }
        
        private void LoadDataTableName(List<string> tableNameList)
        {
            tableNameCombobox.ItemsSource = tableNameList;
        }

        private bool CheckChoooseTable()
        {
            if ((string)tableNameCombobox.SelectedItem == null)
            {
                MessageBox.Show("Please choose your tableName");
                return false;
            }
            return true;
        }
    }
}
