﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace SEPFramework.Views
{
    public partial class HomeWindow : Window
    {

        private Dictionary<int, Dictionary<string, dynamic>> _dataChange = new Dictionary<int, Dictionary<string, dynamic>>(); 
        private void HanldeChangeEvent(object sender, DataGridCellEditEndingEventArgs e)
        {  
            if (e.EditAction == DataGridEditAction.Commit)
            {
                var column = e.Column as DataGridBoundColumn;
                if (column != null)
                {
                    var bindingPath = (column.Binding as Binding).Path.Path;
                    int rowIndex = e.Row.GetIndex();
                    TextBox? el = e.EditingElement as TextBox;
                  
                    if (primaryKeyList.Contains(bindingPath))
                    {
                        MessageBox.Show("Cannot change Data in primary key column");
                        if (rowIndex > dataTableDisplay.Rows.Count - 1)
                        {

                            el.Text = "";
                            return;
                        }
                        el.Text = dataTableDisplay.Rows[rowIndex][bindingPath].ToString();
                        return;
                    }

                    UpdateChangeDataToList(rowIndex, bindingPath, el!.Text);
                }
            }
        }

        private void UpdateChangeDataToList(int index, string colName, dynamic newValue)
        {
            if (_dataChange.ContainsKey(index))
            {
                if (_dataChange[index].ContainsKey(colName))
                {
                    _dataChange[index][colName] = newValue;
                }
                else
                    _dataChange[index].Add(colName, newValue);
            }
            else
            {
                Dictionary<string, dynamic> tmpAddValue = new Dictionary<string, dynamic>();
                tmpAddValue.Add(colName, newValue);
                _dataChange.Add(index, tmpAddValue);
            }
                
        }
    }
}
