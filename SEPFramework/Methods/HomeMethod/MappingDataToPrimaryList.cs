﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SEPFramework.Views
{
    public partial class HomeWindow : Window
    {
        private object MappingDataChangeToPrimaryList(Dictionary<int, Dictionary<string, dynamic>> dataChange)
        {
            // O day save duoi dang key value cua primary key
            // Vd luu danh sach cac primary de cho viec update
            // Ho ten: Thanh
            // Nam sinh : 2018 - 18
            List<Dictionary<string, dynamic>> primaryParams = new List<Dictionary<string, dynamic>>();
            foreach (int index in dataChange.Keys)
            {
                Dictionary<string, dynamic> newPrimarKeyValue = new Dictionary<string, dynamic>();
                foreach (string primaryKeyCol in primaryKeyList)
                {
                    dynamic primaryValue = dataTableDisplay.Rows[index][primaryKeyCol];
                    newPrimarKeyValue.Add(primaryKeyCol, primaryValue);
                }
                primaryParams.Add(newPrimarKeyValue);
            }
            return primaryParams;
        }

        private Dictionary<string, dynamic> MappingRowToPrimaryKeyValue()
        {
            Dictionary<string, dynamic> primaryKeyValue = new Dictionary<string, dynamic>();



            List<string> keys = _selectedRow.Columns.Select(e => e.Header.ToString()).ToList();
            var dataRow = (System.Data.DataRowView)_selectedRow.SelectedItem;
            var values = dataRow.Row.ItemArray;

            foreach (string primaryKey in primaryKeyList)
            {
                if (keys.Contains(primaryKey))
                {
                    if (keys.IndexOf(primaryKey) < values.Length)
                        primaryKeyValue.Add(primaryKey, values[keys.IndexOf(primaryKey)]);
                }
            }
            return primaryKeyValue;
        }

    }
}
