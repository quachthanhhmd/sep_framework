﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SEPFramework.Views
{
    public partial class HomeWindow : Window
    {

        private void ResetDataTable(string tableName)
        {
            UpdateDataGrid(tableName);
            _dataChange.Clear();
        }
    }
}
