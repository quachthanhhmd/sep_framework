﻿using SEPFramework.GeneralController;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SEPFramework.Views
{
    public partial class HomeWindow : Window
    {
        public DataTable dataTableDisplay;
        public List<string> primaryKeyList;
        private void UpdateDataGrid(string tableName)
        {
            DataController dataController = DataController.getControllerInstance();

            dataTableDisplay = dataController.GetDataTable(tableName);
            // MessageBox.Show(dataTableDisplay.Columns[3].DataType.ToString());
            primaryKeyList = dataController.GetPrimaryKey(tableName);
            _dataChange.Clear();

            _selectedRow = null;
            tableData.ItemsSource = dataTableDisplay.DefaultView;
        }
    }
}
