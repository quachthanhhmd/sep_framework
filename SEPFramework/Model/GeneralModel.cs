﻿using ICommandModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEPFramework.Model
{
    public class GeneralModel
    {
        private static GeneralModel _instance;
        private IDataController controller;

        protected IDataController Controller { 
            get {
                if (_instance != null)
                    return _instance.controller;
                return null;
            } }
        protected GeneralModel()
        {
        }

        protected static GeneralModel CreateInstance(IDataController modelController)
        {
            if (_instance == null)
            {
                _instance = new GeneralModel();
                _instance.controller = modelController;
            }
            _instance.controller = modelController;
            return _instance;
        }

        protected static GeneralModel GetModelInstance()
        {
            if (_instance == null)
            {
                return new GeneralModel();
            }
            return _instance;
        }
    }
}
