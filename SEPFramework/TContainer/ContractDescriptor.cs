﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEPFramework.TContainer
{
    public class ContractDescriptor
    {
        public Type typePrimitive  { get; private set; }
        public Type typeImplementation  { get; private set; }
        public object implementation { get; internal set; }
        public ScopeType scope;
        public ContractDescriptor(object implementation, ScopeType scope)
        {
            this.implementation = implementation;
            typePrimitive = implementation.GetType();
            this.scope = scope;

        }

        public ContractDescriptor(Type typeImplement, ScopeType scope)
        {
            typeImplementation = typeImplement;
            this.scope = scope;
        }

        //Use for binding - Builder
        public ContractDescriptor SetPrimitiveType(Type typePrimitve)
        {
            this.typePrimitive = typePrimitve;
            return this;
        }

        public ContractDescriptor SetImplementationType(Type typeImplementation)
        {
            this.typeImplementation = typeImplementation;
            return this;
        }

        public ContractDescriptor AsSigleton()
        {
            this.scope = ScopeType.Singleton;
            return this;
        }

        public ContractDescriptor AsTransient()
        {
            this.scope = ScopeType.Transient;
            return this;
        }




    }

    public enum ScopeType
    {
        Singleton,
        Transient
    }
}
