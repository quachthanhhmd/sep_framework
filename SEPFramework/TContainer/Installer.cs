﻿using SEPFramework.GeneralController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEPFramework.Membership;
using MySQL;
using SEPFramework.Controller;

namespace SEPFramework.TContainer
{
    public static class Installer
    {
        public static TContainer contaier = new TContainer();
        public static void Install()
        {
            contaier.Bind<DataController>().AsSigleton();
            contaier.Bind<Membership.Membership>().AsSigleton();
            contaier.Bind<ICommandModel.IModel, MySQL.SqlServer>().AsTransient();
            contaier.Bind<IAdapterController, DataController>().AsTransient();
            
        }
    }
}
