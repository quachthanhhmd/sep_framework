﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEPFramework.TContainer
{
    public class TContainer
    {
        private Dictionary<Type, ContractDescriptor> _contractDescriptor = new Dictionary<Type, ContractDescriptor>();
        
        //Default is transient
        public ContractDescriptor Bind<TImplementation>()
        {

            if (!_contractDescriptor.ContainsKey(typeof(TImplementation)))
            {
                ContractDescriptor contract = new ContractDescriptor(typeof(TImplementation), ScopeType.Transient);
                _contractDescriptor.Add(typeof(TImplementation), contract);
                return contract;
            }
            return _contractDescriptor[typeof(TImplementation)];
        }

        public ContractDescriptor Bind<TInterface,TImplementation>() where TImplementation : TInterface
        {
            ContractDescriptor contract = new ContractDescriptor(typeof(TImplementation), ScopeType.Transient);
            if (!_contractDescriptor.ContainsKey(typeof(TInterface)))
                 _contractDescriptor.Add(typeof(TInterface), contract);
            return contract;
        }

        public TContract? GetService<TContract>()
        {
            return (TContract)GetImplementation(typeof(TContract));
        }

        private object GetImplementation(Type tContract)
        {
            if (_contractDescriptor.ContainsKey(tContract))
            {
                var contractDescriptor = _contractDescriptor[tContract];

                if (contractDescriptor.implementation != null)  //Singleton
                {
                    return contractDescriptor.implementation;
                }

                //Khi implementation == null thì refelction sẽ CreateInstnace 
                //Uu tien implementation type
                Type actualType = contractDescriptor.typeImplementation ?? contractDescriptor.typePrimitive;
                if (actualType.IsAbstract || actualType.IsInterface)
                {
                    throw new Exception($"Không thể bind interface hoặc abstract class");
                }

                //Pass param to Constructor
                var constructorInfo = actualType.GetConstructors().First();
                var listParams = constructorInfo.GetParameters().Select(x => GetImplementation(x.ParameterType)).ToArray();
                var implementation = Activator.CreateInstance(actualType, listParams);  //Truyền vào 1 implementation cụ thể

                //Nếu nó là singleton thì mới gán - còn ko thì trả về object mới (trasient) và ko gán implementation cho nó
                //K gán => mỗi gần get serveice đều sẽ phải instantiate mới
                if (contractDescriptor.scope == ScopeType.Singleton)
                    contractDescriptor.implementation = implementation;
                return implementation;
            }
            throw new Exception($"Contract này chưa được bind - {tContract}");
        }

        public void DisposeAll()
        {
            _contractDescriptor = null;
            _contractDescriptor = new Dictionary<Type, ContractDescriptor>();
        }



    }
}
