﻿using SEPFramework.CustomViews.CustomButton;
using SEPFramework.Factory;
using SEPFramework.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ConverterDataType;

namespace SEPFramework.Views.FormData
{
    public class AddWindow : BaseWindow
    {
        private string _tableName = null;

        private Membership.Membership _member = Membership.Membership.getInstance();
        private Dictionary<string, object> _newRow = new Dictionary<string, object>();
        private List<Type> _typeDataColList = new List<Type>();

        public delegate void AddTask(bool isSuccess);

        public event AddTask HanldeNotify = null;
        protected override string titleName
        {
            get { return "Add new Data"; }
        }
        protected override void clickSave()
        {
            bool checkValidateSuccess = CheckTypeInput();
            if (!checkValidateSuccess)
            {
                MessageBox.Show("Please check input type.");
                return;
            }
            var tableKeys = $"{_tableName}(\"{String.Join("\",\"", _newRow.Keys)}\")";
            string tableValues = "(";
            for (int i = 0; i < _newRow.Count; i++)
            {
                if (_newRow.ElementAt(i).Value != "")
                    tableValues += ParseValue.ParseInputToQuery(_newRow.ElementAt(i).Value);
                else
                    tableValues += "null";

                if (i != _newRow.Count - 1)
                {
                    tableValues += " , ";
                }
            }
            tableValues += ")";

            bool isSuccess = _member.Add(tableKeys, tableValues);

            if (isSuccess)
            {
                MessageBox.Show("Add new Row Success");
            }
            else
            {
                MessageBox.Show("Add new Row Fail! Please check again.");
            }
            HanldeNotify.Invoke(isSuccess);

            this.Close();
        }

        private bool CheckTypeInput()
        {
            bool isSuccess = true;
            for (int i = 0; i < _newRow.Count; i++)
            {
                object targetValue = null;
                if (ParseValue.ParseType((string)_newRow.ElementAt(i).Value, _typeDataColList[i], ref targetValue))
                {
                    _newRow[_newRow.ElementAt(i).Key] = targetValue;
                }
                else
                {
                    isSuccess = false;
                    break;
                }
            }
            return isSuccess;
        }

        public AddWindow(string tableName, string titleWindow, List<Type> typeDataColList) : base(titleWindow)
        {
            _tableName = tableName;
            InitializeWindow(tableName);
            _typeDataColList = typeDataColList;

            // Cuối cùng là add cái canvas vô cái Window
            this.AddChild(canvas);
        }

        protected override void InitializeWindow(string tableName)
        {

            TextBlock textBlock = base.InitTitleTextBlock(titleName);

            this.canvas.Children.Add(textBlock);

            DataTable data = dataController.GetDataTable(tableName);
            int y = 0;
            foreach (DataColumn item in data.Columns)
            {

                Label tmp = new Label();
                TextBox txt = new TextBox();
                txt.Name = item.ColumnName;
                txt.Width = 100;
                tmp.Content = item.ColumnName;
                labelList.Add(item.ColumnName, tmp);
                textList.Add(item.ColumnName, txt);
                //txt.Height = textBoxHeight;
                txt.Width = textBoxWidth;

                tmp.Margin = new Thickness(150, 60 + y * 30, 0, 0);
                txt.Margin = new Thickness(250, 60 + y * 30, 0, 0);
                y++;

                this.canvas.Children.Add(tmp);
                this.canvas.Children.Add(txt);

                // Add to dict
                _newRow.Add(txt.Name, "");

                // Add action onChanged
                txt.TextChanged += Txt_TextChanged;
            }


            this.save = new MyButton()
                .AddHeight(40)
                .AddWidth(50)
                .AddMargin(new Thickness(200, 80 + y * 30, 0, 0))
                .AddContent(SaveText)
                .AddClickHandle(new RoutedEventHandler(base.Save_Click))
                .Build();

            this.cancel = new MyButton()
                .AddHeight(40)
                .AddWidth(80)
                .AddMargin(new Thickness(100, 80 + y * 30, 0, 0))
                .AddContent(CancelText)
                .Build();

            // Mình sẽ add tất cả các element vô cái canvas
            this.canvas.Children.Add(this.cancel);
            this.canvas.Children.Add(this.save);
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            var txtBox = (TextBox)sender;
            if (_newRow.ContainsKey(txtBox.Name))
            {
                _newRow[txtBox.Name] = txtBox.Text;

                return;
            }

            _newRow.Add(txtBox.Name, txtBox.Text);
        }
    }
}
