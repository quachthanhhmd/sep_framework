﻿using SEPFramework.GeneralController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SEPFramework.Views.FormData
{
    public abstract class BaseWindow : Window
    {
        protected Button save;
        protected Button cancel;

        protected DataController dataController = DataController.getControllerInstance();

        protected string SaveText = "Ok";
        protected string CancelText = "Cancel";
        protected Dictionary<string, Label> labelList = new Dictionary<string, Label>();
        protected Dictionary<string, TextBox> textList = new Dictionary<string, TextBox>();
        public string title = "Base Form";
        protected int textBoxWidth = 150;
        protected int textBoxHeight = 40;
        protected bool hasLabelList = false;
        public Canvas canvas;

        protected abstract string titleName
        {
            get;
        }

        public BaseWindow(string titleWindow)
        {
            this.Width = 500;
            this.Height = 500;
            this.canvas = new Canvas();

            this.title = titleWindow;

            this.save = new Button();
            this.cancel = new Button();
           
        }

        protected void Save_Click(object sender, RoutedEventArgs e)
        {
            this.clickSave();
        }
        protected virtual void clickSave() { }
        protected void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected abstract void InitializeWindow(string tablenName);

        protected TextBlock InitTitleTextBlock(string name)
        {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = name;
            textBlock.Margin = new Thickness(140, 30, 0, 0);
            textBlock.FontSize = 20;
            textBlock.FontWeight = FontWeights.Bold;
            return textBlock;
        }
    }
}
