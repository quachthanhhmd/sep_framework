﻿using SEPFramework.CustomViews.CustomButton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace SEPFramework.Views.FormData
{
    public class DeleteWindow : BaseWindow
    {
        private string _tableName = null;
        // private DataGrid _selectedRow = null;

        private Membership.Membership _member = Membership.Membership.getInstance();
        private Dictionary<string, dynamic> _primaryKeyValue;

        public delegate void DeleteTask(bool isSuccess);
        public event DeleteTask HanldeNotify = null;


        protected override string titleName
        {
            get { return "Delete Data"; }

        }
        protected override void clickSave()
        {
            if (_tableName == null)
            {
                MessageBox.Show("Please choose your tableName");
                return;
            }
            var terms = new List<string>();
            // var keys = _selectedRow.Columns.Select(e => e.Header.ToString()).ToList();
            // var dataRow = (System.Data.DataRowView)_selectedRow.SelectedItem;
            // var values = dataRow.Row.ItemArray;

            foreach (string key in _primaryKeyValue.Keys)
            {
                string condition = "";
                if (_primaryKeyValue[key].GetType().ToString() == "System.String")
                {
                    condition = $"N'{_primaryKeyValue[key]}'";
                }
                else
                {
                    condition = $"{_primaryKeyValue[key]}";
                }
                terms.Add($"{key} = {condition}");
            }

            var deleteCondition = String.Join(" AND ", terms);
            string message = _member.Delete(_tableName, deleteCondition);

            if (message == "")
            {
                MessageBox.Show("Delete Row Success");
            }
            else
            {
                MessageBox.Show(message);
            } 
                
            HanldeNotify?.Invoke(message == "" ? true: false);
            this.Close();
        }
        public DeleteWindow(string tableName, string titleWindow, Dictionary<string, dynamic> primaryKeyValue) : base(titleWindow)
        {
            _tableName = tableName;
            _primaryKeyValue = primaryKeyValue;
            InitializeWindow(tableName);

            // Cuối cùng là add cái canvas vô cái Window
            this.AddChild(canvas);
        }

        protected override void InitializeWindow(string tableName)
        {
            this.save = new MyButton()
                .AddHeight(40)
                .AddWidth(50)
                .AddMargin(new Thickness(200, 80, 0, 0))
                .AddContent(SaveText)
                .AddClickHandle(new RoutedEventHandler(base.Save_Click))
                .Build();

            this.cancel = new MyButton()
                .AddHeight(40)
                .AddWidth(80)
                .AddMargin(new Thickness(100, 80, 0, 0))
                .AddContent(CancelText)
                .Build();

            // Mình sẽ add tất cả các element vô cái canvas
            this.canvas.Children.Add(this.cancel);
            this.canvas.Children.Add(this.save);
        }
    }
}
