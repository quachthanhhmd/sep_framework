﻿using SEPFramework.CustomViews.CustomButton;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SEPFramework.Views.FormData
{
    public class UpdateWindow : BaseWindow
    {
        private string _tableName = null;
        private List<Dictionary<string, dynamic>> _dataChange;
        private List<Dictionary<string, dynamic>> _primaryKeyValueList;

        public delegate void UpdateTask(bool isSuccess);
        public event UpdateTask HanldeNotify = null;


        protected override string titleName
        {
            get { return "Update Data"; }

        }

        protected override void clickSave()
        {
            if (_tableName == null)
            {
                MessageBox.Show("Please select table name");
                return;
            }
            if (_dataChange.Count == 0)
            {
                MessageBox.Show("Please change some feilds");
                return;
            }

            string message = dataController.UpdateData(_tableName, _dataChange, _primaryKeyValueList);

            if (message == "")
            {
                MessageBox.Show("Update Success!!");
            }
            else
            {
                MessageBox.Show(message);
            }


            HanldeNotify?.Invoke(message == "" ? true: false);
            this.Close();

        }
        public UpdateWindow(string tableName, string titleWindow, List<Dictionary<string, dynamic>> dataChange, List<Dictionary<string, dynamic>> primaryKeyValueList) : base(titleWindow)
        {
            _tableName = tableName;
            _dataChange = dataChange;
            _primaryKeyValueList = primaryKeyValueList;

            InitializeWindow(tableName);

            // Cuối cùng là add cái canvas vô cái Window
            this.AddChild(canvas);
        }

        protected override void InitializeWindow(string tableName)
        {

            this.save = new MyButton()
              .AddHeight(40)
              .AddWidth(50)
              .AddMargin(new Thickness(200, 80, 0, 0))
              .AddContent(SaveText)
              .AddClickHandle(new RoutedEventHandler(base.Save_Click))
              .Build();

            this.cancel = new MyButton()
                .AddHeight(40)
                .AddWidth(80)
                .AddMargin(new Thickness(100, 80, 0, 0))
                .AddContent(CancelText)
                .Build();

            // Mình sẽ add tất cả các element vô cái canvas
            this.canvas.Children.Add(this.cancel);
            this.canvas.Children.Add(this.save);
        }

    }
}
