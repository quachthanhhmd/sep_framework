﻿using SEPFramework.Views.FormData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SEPFramework.Membership;
using SEPFramework.GeneralController;
using System.ComponentModel;
using System.Data;

namespace SEPFramework.Views
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    public partial class HomeWindow : Window
    {
        private DataGrid _selectedRow = null;

        public HomeWindow()
        {
            InitializeComponent();
            LoadDataFromDB();
        }

        private void HandleAddWRow(object sender, RoutedEventArgs e)
        {
            if (!CheckChoooseTable())
                return;


            List<Type> typeDataColList  = this.GetTypeOfAllColumn();
            

            AddWindow w1 = new AddWindow((string)tableNameCombobox.SelectedItem, "Add new Data Window", typeDataColList);

            w1.HanldeNotify += (isSucess) =>
            {
                ResetDataTable((string)tableNameCombobox.SelectedItem);
            };

            w1.Show();
        }

        private List<Type> GetTypeOfAllColumn()
        {
            List<Type> typeDataColList = new List<Type>();
            for (int i = 0; i < dataTableDisplay.Columns.Count; i++)
            {
                typeDataColList.Add(dataTableDisplay.Columns[i].DataType);
            }
            return typeDataColList;
        }
        private void HandleUpdateRow(object sender, RoutedEventArgs e)
        {
            if (!CheckChoooseTable())
                return;


            List<Dictionary<string, dynamic>>  primaryKeyValueList = (List<Dictionary<string, dynamic>>)MappingDataChangeToPrimaryList(_dataChange);
            
            // bo cai index
            List<Dictionary<string, dynamic>> dataChangeStandard = new List<Dictionary<string, dynamic>>();

            foreach (Dictionary<string, dynamic> keyValue in _dataChange.Values)
            {
                dataChangeStandard.Add(keyValue);
            }

            UpdateWindow w1 = new UpdateWindow((string)tableNameCombobox.SelectedItem, "Update Data Window", dataChangeStandard, primaryKeyValueList);

            w1.HanldeNotify += (isSucess) =>
            {
                ResetDataTable((string)tableNameCombobox.SelectedItem);
            };

            w1.Show();
                
        }

      

        private void HanleDeleteRow(object sender, RoutedEventArgs e)
        {
            if (!CheckChoooseTable())
                return;

            if (_selectedRow == null)
            {
                MessageBox.Show("Please choose line you want to delete");
                return;
            }
            Dictionary<string, dynamic> primaryKeyValue = MappingRowToPrimaryKeyValue();
           
            DeleteWindow w1 = new DeleteWindow((string)tableNameCombobox.SelectedItem, "Delete Data Window", primaryKeyValue);

            w1.HanldeNotify += (isSucess) =>
            {
                ResetDataTable((string)tableNameCombobox.SelectedItem);
            };
            
            w1.Show();
        }

       
        private void LoadDataFromDB()
        {
            DataController dataController = DataController.getControllerInstance();

            List<string> tableNameList = dataController.GetAllTableName();
            LoadDataTableName(tableNameList);
        }

        private void tableData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _selectedRow = (DataGrid)sender;
        }

        private void buttonLogout(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            this.Close();
            window.Show();
        }
    }
}
