﻿using SEPFramework.GeneralController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SEPFramework.Views
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            object[] inputs = new object[2];
            inputs[0] = username.Text;
            inputs[1] = password.Password;

            if ((string)inputs[1] != confirmPassword.Password)
            {
                MessageBox.Show("Passwords not matched!");
                return;
            }

            if ((string)inputs[0] == "" || (string)inputs[1] == "")
            {
                MessageBox.Show("Username or password must not be blank");
                return;
            }

            DataController dataController = DataController.getControllerInstance();
            if (dataController.CreateSessionRecord(inputs))
            {
                MessageBox.Show("Register OK! Login now");
                this.Close();
            }
            else
            {
                MessageBox.Show("Register failed");
            }
        }
    }
}
