﻿using ICommandModel;
using PasswordAlgorithm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConverterDataType;

namespace SqlServer.Controllers
{
    public class SqlServerController : IDataController
    {
        private IDataModel dataModel;

        private string _databaseName;
        public SqlServerController()
        {
        }

        public SqlServerController(IDataModel model)
        {
            dataModel = model;
            _databaseName = dataModel.getDatabaseName();
        }

        public List<string> GetPrimaryKey(string nameTable)
        {
            string sql = "SELECT u.COLUMN_NAME, c.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS c INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS u ON c.CONSTRAINT_NAME = u.CONSTRAINT_NAME where u.TABLE_NAME = '" + nameTable + "' AND c.TABLE_NAME = '" + nameTable + "' and c.CONSTRAINT_TYPE = 'PRIMARY KEY'";
            try
            {
                DataTable result = dataModel.ExecuteRawQuery(sql);

                List<string> primaryKeyList = new List<string>();
                if (result.Rows.Count > 0)
                {
                    for (int i = 0; i < result.Rows.Count; i++)
                    {
                        primaryKeyList.Add(result.Rows[i]["COLUMN_NAME"].ToString());
                    }
                  
                }

                return primaryKeyList;
            }
            catch (Exception)
            {

            }
            return new List<string>();
        }


        public void createSessionTable()
        {

            DataTable dataTable = dataModel.GetData(string.Format("{0}.INFORMATION_SCHEMA.TABLES", _databaseName), "Table_Name = 'Session'");
            if (dataTable.Rows.Count == 0)
            {
                var createTable = "create table Session(username varchar(30), password varchar(30),isLogin bit,ID INT NOT NULL IDENTITY(1,1) PRIMARY KEY)";
                dataModel.ExecuteQuery(createTable);
            }
        }

        private bool Authen(string username, string password)
        {
            DataTable data = dataModel.GetData("session", string.Format("username='{0}'", username));

            if (data.Rows.Count != 0)
            {
                string u = data.Rows[0]["username"].ToString();
                string p = Crypt.Decrypt(data.Rows[0]["password"].ToString());
                return username == u && password == p;
            }
            return false;
        }

        public bool Login(string username, string password)
        {
            try {
              
                if (Authen(username, password))
                {
                    if (dataModel.Update("session", "isLogin=1", string.Format("username='{0}'", username)) == "")
                        return true;
                }
            } catch (Exception)
            {
                return false;
            }
            return false;
        }

        public List<string> ReadAllDataTableName()
        {
            try
            {
                string queryString = string.Format("USE {0}; SELECT name FROM sys.Tables;", _databaseName);
                DataTable tableNameData = dataModel.ExecuteRawQuery(queryString);

                if (tableNameData.Rows.Count > 0)
                {
                    List<string> tableNamelist = new List<string>();
                    foreach (DataRow row in tableNameData.Rows)
                    {
                        if (row["name"].ToString() != "Session")
                            tableNamelist.Add(row["name"]?.ToString());
                    }
                    return tableNamelist;
                }
                return new List<string>();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }

        public DataTable ReadTable(string nameTable)
        {
            string condition = "1 = 1";
            DataTable result = dataModel.GetData(nameTable, condition);
            return result;
        }

        public bool CreateSessionRecord(object[] inputs)
        {
            string username = (string)inputs[0];
            string password = (string)inputs[1];
            string encryptedPassword = Crypt.Encrypt(password);

            string queryString = string.Format(string.Format("INSERT INTO SESSION(username, password, isLogin) values ('{0}', '{1}', 0)", username, encryptedPassword));
            try
            {
                return dataModel.ExecuteQuery(queryString);
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public DataTable ExecuteRawQuery(string queryString)
        {
            return dataModel.ExecuteRawQuery(queryString);
        }

        public bool Add(string tableName, string value)
        {
            try
            {
                return dataModel.Add(tableName, value);
            } catch (Exception ex)
            {
                return false;
            }
        }

        public string Update(string tableName, string setClause, string condition)
        {
            return dataModel.Update(tableName, setClause, condition);
        }

        public string Delete(string tableName, string condition)
        {
            return dataModel.Delete(tableName, condition);
        }

        public string UpdateRecord(string tableName, List<Dictionary<string, dynamic>> dataChanged, List<Dictionary<string, dynamic>> primaryKeyValue)
        {
           
           for (int i = 0; i < dataChanged.Count; i++)
            {
                string sql = "update " + tableName + " set ";

                for (int j = 0; j < dataChanged[i].Count; j++)
                {

                    if (j < dataChanged[i].Count - 1)
                    {
                        sql += (dataChanged[i].ElementAt(j).Key + " = N'" + dataChanged[i].ElementAt(j).Value + "', ");
                    }
                    else
                    {
                        sql += (dataChanged[i].ElementAt(j).Key + " = N'" + dataChanged[i].ElementAt(j).Value + "'");
                    }
                }

                string primaryStringQuery = " where ";

                for (int j = 0; j < primaryKeyValue[i].Count; j++)
                {
                    primaryStringQuery += $"{primaryKeyValue[i].ElementAt(j).Key} = N'{primaryKeyValue[i].ElementAt(j).Value}'";
                }

                sql += primaryStringQuery;

                try
                {
                    dataModel.ExecuteQuery(sql);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return "";
        }
    }
}
