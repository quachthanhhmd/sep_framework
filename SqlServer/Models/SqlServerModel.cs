﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;

namespace SqlServer.Models
{
    public class SqlServerModel : IDataModel
    {
        private static SqlServerModel _instance;

        public static SqlServerModel GetInstance()
        {
            if (_instance == null)
            {
                _instance = new SqlServerModel();
            }
            return _instance;
        }

        public SqlServerModel()
        {

        }

        private SqlConnection connection = null;

        public SqlServerModel(string url)
        {
            if (_instance == null)
            {
                _instance = new SqlServerModel();
                _instance.connection = new SqlConnection(url);
            }
        }

        public void AttachConnectionString(string url)
        {
            if (_instance == null)
            {
                _instance = new SqlServerModel(url);
                return;
            }
            _instance.connection = new SqlConnection(url);
        }

        public string getDatabaseName()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(_instance.connection.ConnectionString);
            return builder.InitialCatalog;
        }

        public bool CheckIsExist(string queryString)
        {
            SqlCommand query = new SqlCommand(queryString, _instance.connection);
            _instance.connection.Open();

            SqlDataReader result = query.ExecuteReader();

            bool isComplete = result.Read();

            _instance.connection.Close();

            return isComplete;
        }

        public bool ExecuteQuery(string sql)
        {
            try
            {
                _instance.connection.Open();
                SqlCommand sql_query = new SqlCommand(sql, _instance.connection);
                sql_query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                throw ex;
                return false;
            }

            _instance.connection.Close();
            return true;
        }

        public DataTable GetData(string tableName, string condition)
        {
            try
            {
                string queryString = $"select * from {tableName} where {condition}";
                _instance.connection.Open();
                SqlDataAdapter adapt = new SqlDataAdapter(queryString, _instance.connection);

                DataTable dataValue = new DataTable();
                adapt.Fill(dataValue);
                _instance.connection.Close();
                return dataValue;
            }
            catch (Exception)
            {
                _instance.connection.Close();
            }
            return null;
        }

        public bool Add(string tableName, string value)
        {
            string queryString = $"insert into {tableName} values {value}";
            SqlCommand query = new SqlCommand(queryString, _instance.connection);

            try
            {
                _instance.connection.Open();
                query.ExecuteNonQuery();
            }
            catch (Exception)
            {
                _instance.connection.Close();
                return false;
            }

            _instance.connection.Close();
            return true;
        }

        public string Update(string tableName, string setClause, string condition)
        {
            string queryString = $"update {tableName} set {setClause} where {condition}";
            SqlCommand query = new SqlCommand(queryString, _instance.connection);

            try
            {
                _instance.connection.Open();
                query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                return ex.Message;
            }

            _instance.connection.Close();
            return "";
        }

        public string Delete(string tableName, string condition)
        {
            string queryString = $"delete from {tableName} where {condition}";
            SqlCommand query = new SqlCommand(queryString, _instance.connection);

            try
            {
                _instance.connection.Open();
                query.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _instance.connection.Close();
                return ex.Message;
            }

            _instance.connection.Close();
            return "";
        }

        public DataTable ExecuteRawQuery(string queryString)
        {
            try
            {
                _instance.connection.Open();
                SqlDataAdapter adapt = new SqlDataAdapter(queryString, _instance.connection);

                DataTable dataValue = new DataTable();
                adapt.Fill(dataValue);
                _instance.connection.Close();
                return dataValue;
            }
            catch (Exception)
            {
                _instance.connection.Close();

            }
            return new DataTable();
        }
    }
}
