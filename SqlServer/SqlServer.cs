﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICommandModel;
using SqlServer.Controllers;
using SqlServer.Models;

namespace SqlServer
{
    public class SqlServer : IModel
    {
      
        public SqlServer()
        {
              base.DataController = new SqlServerController();
              base.DataModel = SqlServerModel.GetInstance();
        }

        public override string ModelType => "SqlServer";

        public override void AttachConnection(string connectionString)
        {
            DataModel.AttachConnectionString(connectionString);
        }

        public override void AttachModelToController(IDataModel model)
        {
            DataController = new SqlServerController(model);
        }

        
    }
}
